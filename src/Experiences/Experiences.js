import React from 'react'
import learningexp from "../assets/learning-exp.png"
export default function
    () {
    return (
        <div className='container main_background'>
            <div className='main_exp'>
            <div className='left_background'>
                <img src={learningexp} alt='learningexp'></img>
            </div>
            <div className='right_background'>
                <p className='learn_text'>Learn Anywhere, Anytime</p>
                <h2 className='positive_text'>Positive Learning Experiences At Your Fingertips</h2>
                <p className='access_para'>
                    Access digital educational content directly on your mobile device and interact with a learning bot through any one of your preferred social messaging platforms. Come collaborate with peers and educators from around the world, and exchange knowledge and ideas as life-long learners.
                </p>
                <button className='registerBtn'>REGISTER AS LEARNER</button>
            </div>
            </div>
            
        </div>
    )
}
