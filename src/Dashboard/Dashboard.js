import React from 'react'
import Header from './Header'
import bigbglogo from "../assets/big-bg-logo.png"
import bannerimg from "../assets/banner-img.png"
import logomin from "../assets/logo-mini.png"
import textbg from "../assets/text-bg.png"
import Experiences from '../Experiences/Experiences'
export default function Dashboard() {
    return (
        <div>

        
        <div >
           
            <div className='container mainBackground'>
            <Header />
                <div className='main_content'>
                    <div className='left'>
                        <div className='logo_min_section'>
                            <img src={logomin} alt='logomin'>
                            </img>
                        </div>
                        <h2 className='main_text'>
                            Let's build skills <br/> with 
                            <span className='main_ifa_span'>
                                <img src={textbg} alt=''></img>
                                 <h2 className='text_ifa'>
                                IFA</h2></span> & learn without limits...
                                </h2> 
                        <p className='sub_para'>Take your learning to the next level.</p>

                    </div>
                    <div className='right'>
                        <div className='img_overlay'>
                            <img src={bigbglogo} alt='bigbglogo' className='bigbglogo_img'></img>
                            <img src={bannerimg} alt='bannerimg' className='banner_img' >
                            </img>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <Experiences/>
        </div>
    )
}