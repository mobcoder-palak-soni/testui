import React from 'react'
import logo from '../assets/logo-white.png'
import flag_logo from "../assets/united-kingdom.svg"
export default function Header() {
    return (
        <div className='container'>
            <div className='sub_content'>
                <div>
                    <img src={logo} alt='main_logo'></img>
                </div>
                <div>
                    <ul className='menu_text'>
                        <li className='menu_sub_text'>
                            Home
                        </li >
                        <li className='menu_sub_text'>
                            About Us
                        </li>
                        <li className='menu_sub_text'>
                            Courses
                        </li>
                        <li className='menu_sub_text'>
                            Contact
                        </li>
                    </ul>
                </div>
                <div>
                    <ul className='side_menu'>
                        <li>
                            <img src={flag_logo} alt='flag_logo'></img>
                        </li>
                        <li className='login_text'>
                            LOGIN
                        </li>
                        <li>
                            <button className='getStarted_btn'>GET STARTED</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}
